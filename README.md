# Triaxial
Hexagonal grids for Elm 0.19.

## Contents
1. [Installation](#installation)
2. [Usage](#usage)
3. [Documentation](#documentation)
4. [Contributing](#contributing)
5. [Credits](#credits)
6. [License](#license)

## Installation
_Docs missing_

## Usage
_Docs missing_

## Documentation
_Docs missing_

## Contributing
_Docs missing_

## Credits
**Design**
- [Dan's elm-hex-grid](https://github.com/danneu/elm-hex-grid) for Elm 0.18
- [Red Blob Games' article](https://www.redblobgames.com/grids/hexagons) on hexagonal grids

## License
This software and associated documentation files are licensed under the
[MIT License](https://opensource.org/licenses/MIT).

This file may not be copied, modified, or distributed except according to
those terms.
