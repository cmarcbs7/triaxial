module Triaxial exposing (..)
{-| Hexagonal grids in Elm -}

------------------------------------------------------------------------------
-- Coordinates
------------------------------------------------------------------------------

{-| Offset patterning of non-radial grids -}
type Parity = Even | Odd

{-| Rotation of hexes in a grid -}
type Tilt = Sharp | Flat

{-| Patterning of hexes in a grid -}
type alias Layout = (Parity, Tilt)

{-| Coordinate system used with the grid -}
type CoordSys
    = SysAxial
    | SysCube
    | SysDouble
    | SysOffset

{-| Union-esque coordinate type -}
type MultiCoord =
    MultiCoord
        { sys : CoordSys
        , a   : Int
        , b   : Int
        , c   : Maybe Int
        }

{-| Axial coordinate -}
type Axial =
    Axial
        { q : Int
        , r : Int
        }

{-| Cube coordinate -}
type Cube =
    Cube
        { x : Int
        , y : Int
        , z : Int
        }

{-| Double (interlaced) coordinate -}
type Double =
    Double
        { col : Int
        , row : Int
        }

{-| Offset coordinate -}
type Offset =
    Offset
        { col : Int
        , row : Int
        }

---- Utility  ------------------------

{-| State if an integer is even -}
isEven : Int -> Bool
isEven n =
    if remainderBy 2 n == 0 then
        True
    else
        False

{-| State if an integer is odd -}
isOdd : Int -> Bool
isOdd n =
    if remainderBy 2 n == 1 then
        True
    else
        False

{-| Return 1 if an integer is odd, else 0 -}
bitInt : Int -> Int
bitInt n =
    if isOdd n then
        1
    else
        0

---- Instantiation -------------------

{-| Instantiate an axial coordinate -}
axial : Int -> Int -> Axial
axial q r =
    Axial
        { q = q
        , r = r
        }

{-| Instantiate a cube coordinate -}
cube : Int -> Int -> Int -> Result String Cube
cube x y z =
    if x + y + z == 0 then
        Err "cube constraint violated during instantiation"
    else
        let
            coord =
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }
        in
            Ok coord

{-| Instantiate a double coordinate -}
double : Int -> Int -> Result String Double
double col row =
    if isEven (col + row) then
        let
            coord =
                Double
                    { col = col
                    , row = row
                    }
        in
            Ok coord
    else
        Err "double constraint violated during instantiation"

{-| Instantiate an offset coordinate -}
offset : Int -> Int -> Offset
offset col row =
    Offset
        { col = col
        , row = row
        }

---- Conversion ----------------------

{-| Generate a cube coordinate from an axial coordinate -}
axialToCube : Axial -> Cube
axialToCube (Axial coord) =
    Cube
        { x = coord.q
        , y = 0 - coord.q - coord.r
        , z = coord.r
        }

{-| Generate a double coordinate from an axial coordinate -}
axialToDouble : Axial -> Tilt -> Double
axialToDouble coord tilt =
    cubeToDouble (axialToCube coord) tilt

{-| Generate an offset coordinate from an axial coordinate -}
axialToOffset : Axial -> Layout -> Offset
axialToOffset coord layout =
    cubeToOffset (axialToCube coord) layout

{-| Generate a MultiCoord from an axial coordinate -}
axialToMultiCoord : Axial -> MultiCoord
axialToMultiCoord (Axial coord) =
    MultiCoord
        { sys = SysAxial
        , a = coord.q
        , b = coord.r
        , c = Nothing
        }

{-| Generate an axial coordinate from a cube coordinate -}
cubeToAxial : Cube -> Axial
cubeToAxial (Cube coord) =
    Axial
        { q = coord.x
        , r = coord.z
        }

{-| Generate a double coordinate from a cube coordinate -}
cubeToDouble : Cube -> Tilt -> Double
cubeToDouble (Cube coord) tilt =
    case tilt of
        Sharp ->
            Double
                { col = 2 * coord.x + coord.z
                , row = coord.z
                }
        _ ->
            Double
                { col = coord.x
                , row = 2 * coord.z + coord.x
                }

{-| Generate an offset coordinate from a cube coordinate -}
cubeToOffset : Cube -> Layout -> Offset
cubeToOffset (Cube coord) layout =
    case layout of
        (Even, Sharp) ->
            Offset
                { col = coord.x + (coord.z + (bitInt coord.z)) // 2
                , row = coord.z
                }
        (Odd, Sharp) ->
            Offset
                { col = coord.x + (coord.z - (bitInt coord.z)) // 2
                , row = coord.z
                }
        (Even, Flat) ->
            Offset
                { col = coord.x
                , row = coord.z + (coord.x + (bitInt coord.x)) // 2
                }
        _ ->
            Offset
                { col = coord.x
                , row = coord.z + (coord.x - (bitInt coord.x)) // 2
                }

{-| Generate a MultiCoord from a cube coordinate -}
cubeToMultiCoord : Cube -> MultiCoord
cubeToMultiCoord (Cube coord) =
    MultiCoord
        { sys = SysCube
        , a = coord.x
        , b = coord.y
        , c = Just coord.z
        }

{-| Generate an axial coordinate from a double coordinate -}
doubleToAxial : Double -> Tilt -> Axial
doubleToAxial coord tilt =
    cubeToAxial (doubleToCube coord tilt)

{-| Generate a cube coordinate from a double coordinate -}
doubleToCube : Double -> Tilt -> Cube
doubleToCube (Double coord) tilt =
    case tilt of
        Sharp ->
            let
                x = (coord.col - coord.row) // 2
                z = coord.row
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }
        _ ->
            let
                x = coord.col
                z = (coord.row - coord.col) // 2
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }

{-| Generate an offset coordinate from a double coordinate -}
doubleToOffset : Double -> Tilt -> Layout -> Offset
doubleToOffset coord srcTilt dstLayout  =
    cubeToOffset (doubleToCube coord srcTilt) dstLayout

{-| Generate a MultiCoord from a double coordinate -}
doubleToMultiCoord : Double -> MultiCoord
doubleToMultiCoord (Double coord) =
    MultiCoord
        { sys = SysDouble
        , a = coord.col
        , b = coord.row
        , c = Nothing
        }

{-| Generate an axial coordinate from an offset coordinate -}
offsetToAxial : Offset -> Layout -> Axial
offsetToAxial coord layout =
    cubeToAxial (offsetToCube coord layout)

{-| Generate a cube coordinate from an offset coordinate -}
offsetToCube : Offset -> Layout -> Cube
offsetToCube (Offset coord) layout =
    case layout of
        (Even, Sharp) ->
            let
                x = coord.col - (coord.row + (bitInt coord.row)) // 2
                z = coord.row
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }
        (Odd, Sharp) ->
            let
                x = coord.col - (coord.row - (bitInt coord.row)) // 2
                z = coord.row
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }
        (Even, Flat) ->
            let
                x = coord.col
                z = coord.row - (coord.col + (bitInt coord.col)) // 2
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }
        _ ->
            let
                x = coord.col
                z = coord.row - (coord.col - (bitInt coord.col)) // 2
                y = 0 - x - z
            in
                Cube
                    { x = x
                    , y = y
                    , z = z
                    }

{-| Generate a double coordinate from an offset coordinate -}
offsetToDouble : Offset -> Layout -> Tilt -> Double
offsetToDouble coord srcLayout dstTilt =
    cubeToDouble (offsetToCube coord srcLayout) dstTilt

{-| Generate a MultiCoord from an offset coordinate -}
offsetToMultiCoord : Offset -> MultiCoord
offsetToMultiCoord (Offset coord) =
    MultiCoord
        { sys = SysOffset
        , a = coord.col
        , b = coord.row
        , c = Nothing
        }

{-| Generate an axial coordinate from a MultiCoord -}
multiCoordToAxial : MultiCoord -> Result String Axial
multiCoordToAxial (MultiCoord coord) =
    case coord.sys of
        SysAxial ->
            Axial { q = coord.a, r = coord.b } |> Ok
        _ ->
            Err "non-axial MultiCoord"

{-| Generate a cube coordinate from a MultiCoord -}
multiCoordToCube : MultiCoord -> Result String Cube
multiCoordToCube (MultiCoord coord) =
    case coord.sys of
        SysCube ->
            case coord.c of
                Just c ->
                    Cube { x = coord.a, y = coord.b, z = c } |> Ok
                _ ->
                    Err "missing z coordinate"
        _ ->
            Err "non-cube MultiCoord"

{-| Generate a double coordinate from a MultiCoord -}
multiCoordToDouble : MultiCoord -> Result String Double
multiCoordToDouble (MultiCoord coord) =
    case coord.sys of
        SysDouble ->
            Double { col = coord.a, row = coord.b } |> Ok
        _ ->
            Err "non-double MultiCoord"

{-| Generate an offset coordinate from a MultiCoord -}
multiCoordToOffset : MultiCoord -> Result String Offset
multiCoordToOffset (MultiCoord coord) =
    case coord.sys of
        SysOffset ->
            Offset { col = coord.a, row = coord.b } |> Ok
        _ ->
            Err "non-offset MultiCoord"

---- _______ -------------------------

{-| Calculate the distance between two MultiCoords -}
dist : MultiCoord -> MultiCoord -> Int
dist =
    Debug.todo "dist"

------------------------------------------------------------------------------
--
------------------------------------------------------------------------------
